idBoAtual = localStorage.getItem('idBo');

$(document).ready(function() {
    consultarBo()

});

var listarLinks = function() {
}

var consultarBo = function() {
    idAmostra = localStorage.getItem('idAmostra')
    idBo = localStorage.getItem('idBo')

    $.ajax({
        url: "http://localhost:8080/criminal-report-api/report/" + idBo,
        type: 'GET',
        async: true,
		crossDomain:true,
        contentType: 'application/json',
        success: function(bo) {
			//TODO
            //localStorage.setItem('idBo', bo.idNext);
            $("#local").text(bo.local);
            $("#tipoLocal").text(bo.tipoLocal);
            $("#categoria").text(bo.categoria);
            $("#turnoOcorrencia").text(bo.turnoOcorrencia);
            $("#dataOcorrencia").text(bo.dataOcorrencia);

            partes = bo.partesEnvolvidas;
            $.each(partes, function(index, parte) {
                var str = "<div class=\"parte\">\n";
                str += "Name:<b>" + parte.nome + "</b><br>";
                str += "Involvement type:" + parte.tipoEnvolvimento + "<br>";
                str += "Person ID (RG):" + parte.rg + "<br>";
                str += "Place of birth:" + parte.naturalidade + "<br>";
                str += "Country:" + parte.nacionalidade + "<br>";
                str += "Gender:" + parte.sexo + "<br>";
                str += "Date of birth:" + parte.dataNascimento + "<br>";
                str += "Marital status:" + parte.estadoCivil + "<br>";
                str += "Education:" + parte.instrucao + "<br>";
                str += "Occupation:" + parte.profissao + "<br>";
                str += "Skin color:" + parte.cutis + "<br>";
                str += "Nature involved:" + parte.naturezasEnvolvidas + "<br>";
                str += "Age:" + parte.idade + "<br>";
                str += "<br><br>";
                str += "<div id='expansaoTJSP-"+parte.rg+"' style='border:thin; border:1px; solid black'></div>";
		        str += "<br><br>";
				
				
		        $.ajax({
		            url: "http://localhost:8081/criminal-report-evaluation-api/sample/"+ idAmostra +"/" + idBo + "/tjsp/",
		            type: 'GET',
		            async: true,
		            contentType: 'text/html',
		            success: function(expansao) {
			            console.log(xhr.responseText)
			            $("#expansaoTJSP-"+ parte.rg).html("<b>TJSP:<br>"+xhr.responseText+"<b>");
		            },
		            error: function(xhr, status, error) {
			            console.log(xhr.responseText)
			            $("#expansaoTJSP-"+ parte.rg).html("<b>TJSP:<br>"+xhr.responseText+"<b>");
		            }
	            }); 
				

                str += "\n</div>";
                $(".partes").append(str);
            });
            
			
            for (var i = 1; i <= 1; ++i) {
                var str = "<span style=\"font-weight: bold;\">";
                str += i + "&ordm; Resultado do Google</span><br></br>";
                str += "<span id=\"googleUrl\" style=\"font-family: monospace;\" ></span><br></br>";
                str += "<iframe id=\"google\" width=\"100%\" height=\"400px\"></iframe><br></br>";
                document.getElementById("googleDiv").innerHTML += str;

                $.ajax({
                    url: "http://localhost:8081/criminal-report-evaluation-api/sample/" + idAmostra + "/" + idBo + "/google/",
                    type: "GET",
                    async: true,
                    error: function() {},
                    success: (function(i) {
                        return function(html) {
                            document.getElementById("google").contentWindow.document.write(html);
                        };
                    })(i)
                });
                
                $.ajax({
                    url: "http://localhost:8081/criminal-report-evaluation-api/sample/" + idAmostra + "/" + idBo + "/tjsp",
                    type: "GET",
                    async: true,
                    error: function() {},
                    success: (function(i) {
                        return function(url) { 
                            console.error("url:" + url);
                            document.getElementById("googleUrl").innerHTML = url;
                        };
                    })(i)
                });
                
            }
			
            
        },
        error: function() {}
    });
}


$("#linkProximo").click(function() {
    avaliar();
});


var avaliar = function() {
    opcaoGoogle = $('input[name=google]:checked').val()
    opcaoTjsp = $('input[name=tjsp]:checked').val()

    avaliacao = {
        avaliacaoGoogle: opcaoGoogle,
        avaliacaoTJSP: opcaoTjsp
    }

    idAmostra = localStorage.getItem('idAmostra')

    $.ajax({
        url: "http://localhost:8081/criminal-report-evaluation-api/sample/assessment/idSample=" + idAmostra + "&idReport=" + idBoAtual + "&resultGoogle=" + opcaoGoogle+ "&resultTJSP=" + opcaoTjsp,
        type: 'POST',
        async: true,
        contentType: 'application/json',
        data: JSON.stringify(avaliacao),
        success: function() {
            window.location.href = "avaliacaoBo.html";
        },
        error: function(xhr, status, error) {

        }
    });

}
